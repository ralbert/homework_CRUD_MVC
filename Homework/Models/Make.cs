using System;

namespace Homework.Models {
    public class Make {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}