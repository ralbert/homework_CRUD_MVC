using Microsoft.EntityFrameworkCore;

namespace Homework.Models {
    public class CarContext : DbContext {
        public CarContext (DbContextOptions<CarContext> options) : base (options) { }
        public DbSet<Homework.Models.Car> Car { get; set; }
        public DbSet<Homework.Models.Make> Make { get; set; }
    }
}