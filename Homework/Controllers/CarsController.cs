using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc;

namespace Homework.Controllers {
    public class CarsController : Controller {
        // 
        // GET: /HelloWorld/

        public string Index () {
            return "This is my default action...";
        }

        // 
        // GET: /HelloWorld/Welcome/ 

        public string Welcome () {
            return "This is the Welcome action method...";
        }
    }
}